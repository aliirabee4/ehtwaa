import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final bool secure;
  CustomTextField({this.hint, this.secure});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      child: TextFormField(
        obscureText: secure,
        decoration: InputDecoration(
            hintText: hint,
            contentPadding: EdgeInsets.all(5),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10))),
      ),
    );
  }
}
