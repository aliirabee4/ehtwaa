import 'dart:ui';

import 'package:ehtwaa/screens/bottom_navigation/view.dart';
import 'package:ehtwaa/screens/signup/view.dart';
import 'package:ehtwaa/ui_widgets/custom_button.dart';
import 'package:ehtwaa/ui_widgets/text_field.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'EcoCulture',
          style: TextStyle(color: Colors.greenAccent, fontSize: 22),
        ),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              children: [
                Text(
                  'Sign In',
                  style: TextStyle(
                      color: Colors.greenAccent,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          CustomTextField(
            hint: 'Email address',
            secure: false,
          ),
          CustomTextField(
            hint: 'Password',
            secure: true,
          ),
          SizedBox(
            height: 50,
          ),
          CustomButton(
            title: 'Login',
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BottomNavigationScreen()));
            },
            color: Colors.amber,
          ),
          CustomButton(
            title: 'Create Account',
            color: Colors.grey,
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SignUpScreen()));
            },
          ),
        ],
      ),
    );
  }
}
